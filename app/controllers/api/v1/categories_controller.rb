class Api::V1::CategoriesController < ApplicationController
  def index
  	@category=Category.all
  	if @category.present?
  		render json: {status: true,message: "List of all category",category: ActiveModel::ArraySerializer.new(@category, each_serializer:  CategorySerializer)},code: 200
  	else
  		render json: {status: true,message: "No category found"},code: 200
  	end 
  end

  def show
     @category=Category.find_by(id: params[:id])
     if @category.present?
       render json: {status: true,message: "detail of category",category: CategorySerializer.new(@category,root: false)},code: 200
     else
       render json: {status: false,message: "there is category of this id"},code: 404
     end
  end

  def update
    @category=Category.find_by(id: params[:id])
    @category.name=params[:categories][:name]
    if params[:categories][:name].present?
     if @category.save
       render json: {status: true,message: "updates category",category: CategorySerializer.new(@category,root: false)},code: 200
     else
       render json: {status: false,message: "unable to update category"},code: 404
     end
    else
      render json: {status: false,message: "data is missing"},code: 404
    end
  end
  def destroy
    @category=Category.find_by(id: params[:id])
    if @category.present?
      if @category.destroy
        render json: {status: true,message: "category deleted"},code: 200
      else
        render json: {status: false,message: "unable to delete category"},code: 404
      end
    else
      render json: {status: false,message: "no category found"},code: 404
    end
  end
  def create
    if params[:categories][:name].present?
      @category=Category.create(name: params[:categories][:name])
      render json: {status: true, message: "category created successfully",category: CategorySerializer.new(@category, root: false)}, code: 200
    else
      render json: {status: false, message: "No category created"}, code: 400
    end
  end
  protected
  def category_params
    params.require(:categories).permit(:name)
  end
end
