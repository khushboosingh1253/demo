class Api::V1::ProductsController < ApplicationController
  def index
  	if params[:category_id].present?
  		@category = Category.find_by(id: params[:category_id])
  		if @category.present?
  			@products=@category.products
  			render json: {status: true,message: "list of product in this category",products: ActiveModel::ArraySerializer.new(@products,each_serializer: ProductSerializer)},code: 200
  		else
  		  render json: {status: false,message: "no category is available of this id"},code: 404
  		end
  	else
  		render json: {status: false,message: "category id is missing"},code: 404
  	end
  end

  def show
  	@product = Product.find_by(id: params[:id])
  	if @product.present?
		render json: {status: true,message: "Product detail",product: ProductSerializer.new(@product,root: false)},code: 200
	else
	  render json: {status: false,message: "no Product is available of this id"},code: 404
	end
  end
  def destroy
  	@product = Product.find_by(id: params[:id])
  	if @product.destroy
		render json: {status: true,message: "deleted successfully"},code: 200
	else
	  render json: {status: false,message: "something wents wrong"},code: 404
	end
  end
  protected
  def category_params
  	params.require.(:categories).permit(:name)
  end
  def product_params
  	params.require(:products).permit(:name)
  end
end
