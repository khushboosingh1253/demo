class Api::V1::SocialOuthsController < ApplicationController
  def create
  	if params[:device_id].present?&&params[:uid].present?&&params[:provider].present?||params[:email].present?
      if params[:email].present?
        user=User.find_by(email: params[:email])
        if user.present?
          user.avatar=params[:file]
          user.save
          social_outh=user.social_ouths.find_or_create_by(uid: params[:uid],provider: params[:provider])
          device_id_generate(user,params[:device_id])
        else
          random_token = SecureRandom.hex(8)
          user = User.create(email: params[:email],password: random_token,avatar: params[:file].to_s)
          social_outh= user.social_ouths.create(provider: params[:provider],uid: params[:uid] )
          device_id_generate(user,params[:device_id])
          
        end
      else
        social_outh=SocialOuth.find_by(uid: params[:uid])
        if  social_outh.present?
            user=User.find_by(id: social_outh.try(:user_id))
            user.avatar=params[:file]
            user.save
            device_id_generate(user,params[:device_id])
        else
          random_token = SecureRandom.hex(8)
          user = User.create(email: "",password: random_token,avatar: params[:file].to_s)
          social_outh= user.social_ouths.create(provider: params[:provider],uid: params[:uid] )
          device_id_generate(user,params[:device_id])
        end
      end
    else
      render json: {status: false,message: "required data is missing"},code: 404
    end
  end

private
  def device_id_generate(user, device_id) 
          is_device_id_unique=user.unique_device(device_id)
          if is_device_id_unique
              device=user.user_devices.find_or_create_by(device_id: device_id)   
              if device
              session["device_id"]=device_id
              user.skip_confirmation!
              sign_in(user)
              render json: {status: true, message: "Sign in succesfull.",user: UserSerializer.new(user, root: false)}, code: 200
              else
              render json: {status: false, message: user.errors.full_messages.join(', ')}, code: 200
             end
          else
              render json: {status: true, message: "Device session is already running"}, code: 200
          end
 
  end
 
end
