class Api::V1::Users::PasswordsController < Devise::PasswordsController
	def create
		user=User.find_by(email: params[:api_v1_user][:email])
		if user.present?
		  user.generate_otp
          Devise::Mailer.reset_password_instructions(user, user.reset_password_token, opts={}).deliver_now
          render json: {status: true, message: "An email has been set with reset password instructions."}, code: 200
		else
			render json: {status: false, message: "User does not exist"}, code: 200
		end
	end
	def update
      if (params[:api_v1_user][:reset_password_token].present? && params[:api_v1_user][:password].present?)
        user = User.find_by(reset_password_token: password_reset_params[:reset_password_token])
        if user.present?
          user.update_attributes(password: password_reset_params[:password], reset_password_token: nil)  
          render json: {status: true, message: 'Password updated successfully'}, code: 200
        else
          render json: {status: false, message: "Invalid OTP"}, code: 200
        end
      else
        render json: {status: false, message: "Please provide required parameters"}, code: 200
      end
    end
    def password_reset_params
      params.require(:api_v1_user).permit(:reset_password_token, :password)
    end
end
