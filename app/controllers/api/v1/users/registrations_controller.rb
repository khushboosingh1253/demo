class Api::V1::Users::RegistrationsController < Devise::RegistrationsController
	def create
	  if params[:api_v1_user][:email].present? && params[:api_v1_user][:password].present?
	  	user=User.find_by(email:  params[:api_v1_user][:email])
	  	if user.present?
	  		render json: {status: false, message: "Already registed with this email"}, code: 200
	  	else
			user=User.new(user_params)
			if user.save
				render json: {status: true, message: "Registation successfull"}, code: 200
			else
				render json: user.errors and return
			end
		end
	  else
	  	render json: {status: false, message: "please enter required data"}, code: 200
	  end
	end

	protected

    def user_params
    	params.require(:api_v1_user).permit(:email, :password)
    end
end