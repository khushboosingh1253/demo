class Api::V1::Users::SessionsController < Devise::SessionsController
  def create
    if params[:users][:email].present? && params[:users][:password].present?
      user = User.find_user(params[:users][:email])
      if user.present?
        if user.valid_password? params[:users][:password]
           is_device_id_unique=user.unique_device(params[:device_id])
           if is_device_id_unique
             device=user.user_devices.find_or_create_by(device_id: params[:device_id])   
             if device
             session["device_id"]=params[:device_id]
             sign_in(user)
             render json: {status: true, message: "Sign in succesfull.",user: UserSerializer.new(user, root: false)}, code: 200
             else
              render json: {status: false, message: user.errors.full_messages.join(', ')}, code: 200
             end
           else
             render json: {status: true, message: "Device session is already running"}, code: 200
           end
        else
            render json: {status: true, message: 'Please provide valid password'}, code: 200
        end
      else
        render json: {status: true, message: 'user does not exist'}, code: 200
      end
    else
      render json: {status: false, message: 'Please provide required parameters'}, code: 200
    end
  end
  def destroy
  device_id=session["device_id"]
  user=User.find_by(id: current_api_v1_user.try(:id))
  @device = user.user_devices.find_by(device_id: device_id)
  @device.destroy
  
  
  if @device.destroyed?
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(@user))
    render json: {status: true, message: 'Sign out succesfully'},code: 200
  else
    render json: {status: false, message: @device.errors.full_messages.join(', ')}, code: 20
  end
      
  end
  protected

  def user_params
    params.require(:users).permit(:auth_token, :password, :device_id)
  end
end