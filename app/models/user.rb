class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  mount_base64_uploader :avatar, AvatarUploader
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,:confirmable
  has_many :user_devices,:dependent => :destroy 
    accepts_nested_attributes_for :user_devices
  has_many :social_ouths,:dependent => :destroy 
  def self.find_user(email)
    return self.find_by(email: email)
  end
  def email_required?
    false
  end
  def create_devise_id devise_id
    device=self.user_devices.find_or_create_by(device_id: devise_id)
    return generate_auth_token(device)
  end
  def generate_auth_token device
    random_token= SecureRandom.hex(8)
    device.auth_token=random_token
    device.save
  end
  def unique_device devise_id
    unique_device=UserDevice.where("device_id = ? AND id IN (?)",devise_id,self.user_devices.pluck(:id))
    if unique_device.present?
      return false
    else 
      return true
    end
      
  end
  def destroy_current_session(current_user_id,devise_id)
    user= User.find(current_user_id)
    @user_devices = user.user_devices.find_by(device_id: devise_id)
    return @user_devices
  end

  def generate_otp
      self.reset_password_token = loop do
        random_token = SecureRandom.random_number(100000)
        if (random_token.to_s.length == 5)
          break random_token unless User.exists?(reset_password_token: random_token)
        end
      end
      self.save
  end 
end
