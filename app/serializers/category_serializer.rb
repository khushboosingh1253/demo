class CategorySerializer < ActiveModel::Serializer
  attributes :id,:name,:products
  def products
  	object.products.select(:id,:name)
  end
end
