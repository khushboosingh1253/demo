Rails.application.routes.draw do
  

  

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root 'home#index'
  get 'home/index'=>'home#index'

  namespace :api do
  	namespace :v1 do
      resources :categories
      resources :products
      resources :profiles
      resources :social_ouths do
        collection do 
         match "device_id_generate" => "social_ouths#device_id_generate" , as: "device_id_generate", via: [:get]
        end
      end
  		devise_for :users, controllers:{
  			sessions: 'api/v1/users/sessions',
  			registrations: 'api/v1/users/registrations',
  			confirmations: 'api/v1/users/confirmations',
  			passwords: 'api/v1/users/passwords'
  		}
  	end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
