class CreateUserDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :user_devices do |t|
      t.belongs_to :user
      t.string :device_id, unique: true
      t.string :auth_token, unique: true
      t.timestamps
    end
  end
end
