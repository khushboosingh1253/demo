class CreateSocialOuths < ActiveRecord::Migration[5.1]
  def change
    create_table :social_ouths do |t|
      t.belongs_to :user
      t.string :provider
      t.string :uid
      t.timestamps
    end
  end
end
